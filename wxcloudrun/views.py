import json
import logging
import smtplib
from email.header import Header

from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from smtplib import SMTP_SSL
from email.mime.text import MIMEText

logger = logging.getLogger('log')


def data(request):
    """
    获取主页

     `` request `` 请求对象
    """
    response = render(request, 'data.html')
    # response['Content-Security-Policy'] = 'frame-ancestors "self"'
    response['X-Frame-Options'] = ''
    return response


def dataen(request):
    """
    获取主页

     `` request `` 请求对象
    """
    response = render(request, 'dataen.html')
    # response['Content-Security-Policy'] = 'frame-ancestors "self"'
    response['X-Frame-Options'] = ''
    return response


def dataSchools(request):
    """
    获取主页

     `` request `` 请求对象
    """
    response = render(request, 'dataschools.html')
    # response['Content-Security-Policy'] = 'frame-ancestors "self"'
    response['X-Frame-Options'] = ''
    return response


def send_smtp(request):
    """
       更新计数，自增或者清零

       `` request `` 请求对象
       """

    logger.info('update_count req: {}'.format(request))
    # print(request)
    email = request.POST['email']
    wxcode = request.POST['wxcode']
    name = request.POST['name']
    timeZone = request.POST['timeZone']
    channel = request.POST['channel']
    # message = ''' <p>''' '邮箱：' + email + ''' </p>''' + ''' <p>''' + '微信账号：' + wxcode + ''' <p/>''' + ''' <p>''' + \
    #           '学生称呼：' + name + ''' <p/>''' + ''' <p>''' + '时区：' + timeZone + ''' <p/>''' + '了解渠道：' + channel

    message = '邮箱：' + email + '\n微信账号：' + wxcode + \
              '\n学生称呼：' + name + '\n时区：' + timeZone + '\n了解渠道：' + channel
    # print(message)
    Subject = '报名学员'
    # 显示发送人
    sender_show = 'emily'
    # 显示收件人
    recipient_show = 'emily'
    # 实际发给的收件人
    to_addrs = '35295618@qq.com'

    sendMail(message, Subject, sender_show, recipient_show, to_addrs)
    # send_email_Outlook()


def send_smtp_en(request):
    """
       更新计数，自增或者清零

       `` request `` 请求对象
       """

    logger.info('update_count req: {}'.format(request))
    # print(request)
    email = request.POST['email']
    name = request.POST['name']
    timeZone = request.POST['timeZone']
    channel = request.POST['channel']
    message = 'Email：' + email + \
              '\nStudent Name：' + name + '\nTime Zone：' + timeZone + '\nchannel：' + channel
    print(message)
    Subject = 'Trial class application '
    # 显示发送人
    sender_show = 'emily'
    # 显示收件人
    recipient_show = 'emily'
    # 实际发给的收件人
    to_addrs = '35295618@qq.com'
    sendMail(message, Subject, sender_show, recipient_show, to_addrs)


def send_smtp_schools(request):
    """
       更新计数，自增或者清零

       `` request `` 请求对象
       """

    logger.info('update_count req: {}'.format(request))
    # print(request)
    yourName = request.POST['yourName']
    email = request.POST['email']
    phone = request.POST['phone']
    schoolName = request.POST['schoolName']
    state = request.POST['state']
    country = request.POST['country']
    channel = request.POST['channel']
    message = 'Your Name：' + yourName + '\nEmail：' + email + '\nPhone：' + phone + '\nYour school name：' + schoolName + \
              '\nstate：' + state + '\nCountry：' + country + '\nchannel：' + channel

    print(message)
    Subject = 'Trial class application '
    # 显示发送人
    sender_show = 'emily'
    # 显示收件人
    recipient_show = 'emily'
    # 实际发给的收件人
    to_addrs = '35295618@qq.com'
    sendMail(message, Subject, sender_show, recipient_show, to_addrs)


def send_email_Outlook(subject="24zbw推送提醒", content="24zbw推送集锦录像失败，请查看", recver="emily@preplingo.com"):
    # 第三方 SMTP 服务
    mail_host = "smtp.office365.com"  # 设置服务器

    mail_user = "emily@preplingo.com"  # 用户名

    mail_pass = "Password1"  # 口令

    sender = 'emily@preplingo.com'

    # receivers = ['test@qq.com']

    # 调试开启 写死 我的邮件

    receivers = recver  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    message = MIMEText(content, 'plain', 'utf-8')

    # message['From'] = Header("24脚本", 'utf-8')

    # message['To'] =  Header("24脚本接收端", 'utf-8')

    # subject = subject

    message['Subject'] = Header(subject, 'utf-8')

    # try:

    smtp = smtplib.SMTP(host=mail_host, port=587)

    smtp.starttls()  # 明文通信协议的扩展，能够让明文的通信连线直接成为加密连线（使用SSL或TLS加密），而不需要使用另一个特别的端口来进行加密通信，属于机会性加密

    smtp.login(mail_user, mail_pass)

    smtp.sendmail(sender, receivers, message.as_string())

    print('邮件发送成功')


def sendMail(message, Subject, sender_show, recipient_show, to_addrs, cc_show=''):
    '''
    :param message: str 邮件内容
    :param Subject: str 邮件主题描述
    :param sender_show: str 发件人显示，不起实际作用如："xxx"
    :param recipient_show: str 收件人显示，不起实际作用 多个收件人用','隔开如："xxx,xxxx"
    :param to_addrs: str 实际收件人
    :param cc_show: str 抄送人显示，不起实际作用，多个抄送人用','隔开如："xxx,xxxx"
    '''
    # 填写真实的发邮件服务器用户名、密码
    user = '35295618@qq.com'
    password = 'ptyizeqcjpsgbigi'
    # 邮件内容
    msg = MIMEText(message, 'plain', _charset="utf-8")
    # 邮件主题描述
    msg["Subject"] = Subject
    # 发件人显示，不起实际作用
    msg["from"] = sender_show
    # 收件人显示，不起实际作用
    msg["to"] = recipient_show
    # 抄送人显示，不起实际作用
    msg["Cc"] = cc_show
    with SMTP_SSL(host="smtp.qq.com", port=465) as smtp:
        # 登录发邮件服务器
        smtp.login(user=user, password=password)
        # 实际发送、接收邮件配置
        smtp.sendmail(from_addr=user, to_addrs=to_addrs.split(','), msg=msg.as_string())

    return JsonResponse({'code': 200, 'data': data.count},
                        json_dumps_params={'ensure_ascii': False})
