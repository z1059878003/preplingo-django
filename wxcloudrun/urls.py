"""wxcloudrun URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from wxcloudrun import views
from django.urls import path

urlpatterns = (

    path('api/data/cn', views.data),
    path('api/data/en', views.dataen),
    path('api/data/schools', views.dataSchools),
    # 发送邮箱cn
    path('api/smtp', views.send_smtp),
    # 发送邮箱en
    path('api/smtp/en', views.send_smtp_en),
    # 发送邮箱schools
    path('api/smtp/schools', views.send_smtp_schools),

)
